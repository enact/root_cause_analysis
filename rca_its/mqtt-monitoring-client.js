const mqtt = require('mqtt');
const fs = require('fs');
const path = require('path');
const MongoClient = require('mongodb').MongoClient;
const url = "mongodb://localhost:27017/";

const connConfig = {
  host: '34.255.17.2',
  port: 8886,
  username: 'lnguyen',
  password: 'Enact2019',
  rejectUnauthorized: false,
  ca: fs.readFileSync(path.join(__dirname,'indra-certs/Public-ca-chain.cert.pem')),
  cert: fs.readFileSync(path.join(__dirname,'indra-certs/lnguyen.cert.pem')),
  key: fs.readFileSync(path.join(__dirname,'indra-certs/lnguyen.key.pem')),
  protocol: 'mqtts'
};

const client = mqtt.connect(connConfig);

client.subscribe('109/+/+/+/500/+/101/#');
// client.publish('messages', 'Current time is: ' + new Date())
client.on("message", onMessageReceived);

function seconds_since_epoch(d){  
    return Math.floor( d / 1000 );  
} 

function insert_to_raw_data_real_time(curr_state){
	MongoClient.connect(url, function(err, db) { //insert to the database of raw data real time
		if (err) return 0;
		else{
			var dbo = db.db("mmt-rca");
		    if (dbo.collection("raw_data_real_time").find({"_id":curr_state["_id"]}).limit(1).length === 1)
				return 0;
		    else {
		    	dbo.collection("raw_data_real_time").insertOne(curr_state, function(error, res) {
		    	if (error) return 0;
		    });
		    }
		    db.close();
		}
	});
	//console.log("Inserted");
	return 1;
}

function report_to_INDRA_broker(gw, TimeStamp, TimeAccuracy){
	//comment out from here if not reporting to indra
	MongoClient.connect(url, function(err, db) { //insert to the database of raw data real time
		if (err) return 0; 
		var dbo = db.db("mmt-rca");
		dbo.collection('109102_report', function(err, collection) {
			  collection
			    .find()
			    .sort({$natural: -1})
			    .limit(1)
			    .next()
			    .then(
			      function(doc) {
			        console.log(doc);
			        console.log("Created: ", doc["Created"]);
			    	console.log("ID: ", doc["KnownIncidentID"]);
			    	console.log("Description: ", doc["Description"]);
			    	console.log("Similarity score: ", doc["Similarity score"]);
			    	console.log("Proof: ", doc["Proof"]);
			    	var report = {
			    			"ServiceID":109102,
			    			"Root":{
			    				"Gateway":gw,
			    				"Source":0,
			    				"TimeStamp":TimeStamp},
			    			"Nodes":[{
			    				"Safety":true,
			    				"NodeID":1,
			    				"TimeStamp":TimeStamp,
			    				"TimeAccuracy":TimeAccuracy,
			    				"Sensors-Actuators":[
			    					{
			    					"SensorID":3333,
			    					"TimeStamp":TimeStamp,
			    					"TimeAccuracy":TimeAccuracy,
			    					"Resources":{
			    						"5506":doc["Created"]}
			    					},
			    					{
			    					"SensorID":3341,
			    					"TimeStamp":TimeStamp,
			    					"TimeAccuracy":TimeAccuracy,
			    					"Resources":{
			    						"5527": doc["KnownIncidentID"]
			    							}
			    					},
			    					{
			    					"SensorID":3341,
			    					"TimeStamp":TimeStamp,
			    					"TimeAccuracy":TimeAccuracy,
			    					"Resources":{
			    						"5527": doc["Description"]
			    							}
			    					},
			    					{
			    					"SensorID":3300,
			    					"TimeStamp":TimeStamp,
			    					"TimeAccuracy":TimeAccuracy,
			    					"Resources":{
			    						"5700": doc["Similarity score"]
			    						}
			    					},
			    					{
			    					"SensorID":3341,
			    					"TimeStamp":TimeStamp,
			    					"TimeAccuracy":TimeAccuracy,
			    					"Resources":{
			    						"5527": doc["Proof"]
			    							}
			    					},
			    					{
			    					"SensorID":3333,
			    					"TimeStamp":TimeStamp,
			    					"TimeAccuracy":TimeAccuracy,
			    					"Resources":{
			    						"5506":doc["Created"]
			    						}
			    					}
			    					],
			    					"CRC":3285226955}],
			    				"CRC":2986263130
			    				};
			    	//console.log(report);
			    	client.publish('109/102/0/0/124/100/100/0/0', JSON.stringify(report))
			      },
			      function(err) {
			        console.log('Error:', err);
			      }
			    );
			});
		db.close();
	});
	return 1;
}

var curr_state = {	
};

function onMessageReceived(topic, message) {
	var data = JSON.parse(message.toString());
	var ServiceID = data.ServiceID;
	var gw = data.Root.Gateway;
	var gw_missing = "1000502"
    if (ServiceID == 109101){ //Hardware status 
		var ts = new Date();
		var TimeStamp = seconds_since_epoch(ts);
	    var TimeAccuracy = 1000000*(Number(ts)-seconds_since_epoch(ts)*1000)
	    curr_state["_id"] = "STATE" + Number(ts).toString();
	    curr_state["timestamp"] = Number(ts); //in milliseconds
		/*
		if (curr_state["_id"] == "STATE" + Number(ts).toString())
    	{
    	curr_state["_id"] = "STATE" + (Number(ts) + 1).toString();
    	}
		else 
    	{
    	curr_state["_id"] = "STATE" + Number(ts).toString();
    	}
    	*/
		//console.log(ServiceID, data.Root.Gateway);
		//console.log(data.Nodes[0]['Sensors-Actuators'][0].Resources['5700']);
		//curr_state["timestamp"] = Number(data.Nodes[0]['Sensors-Actuators'][0]['TimeStamp']);
		curr_state[data.Root.Gateway.toString() + "_CPU"] = Number(data.Nodes[0]['Sensors-Actuators'][0].Resources['5700']);
		curr_state[data.Root.Gateway.toString() + "_RAM"] = Number(data.Nodes[0]['Sensors-Actuators'][1].Resources['5700']);
		curr_state[data.Root.Gateway.toString() + "_POW"] = Number(data.Nodes[0]['Sensors-Actuators'][2].Resources['5700']);
		curr_state[data.Root.Gateway.toString() + "_DISK"] = Number(data.Nodes[0]['Sensors-Actuators'][3].Resources['5700']);
		
		//gateway missing
		curr_state[gw_missing + "_CPU"] = 0;
		curr_state[gw_missing + "_RAM"] = 0;
		curr_state[gw_missing + "_POW"] = 0;
		curr_state[gw_missing + "_DISK"] = 0;
		if (Object.keys(curr_state).length == 123){
			insert_to_raw_data_real_time(curr_state);
			report_to_INDRA_broker(gw, TimeStamp, TimeAccuracy);
		}
	}
	else if (ServiceID == 109100){ //Network connection status
		var ts = new Date();
		var TimeStamp = seconds_since_epoch(ts);
	    var TimeAccuracy = 1000000*(Number(ts)-seconds_since_epoch(ts)*1000)
	    curr_state["_id"] = "STATE" + Number(ts).toString();
	    curr_state["timestamp"] = Number(ts); //in milliseconds
		/*
		if (curr_state["_id"] == ("STATE" + Number(ts).toString()))
    	{
    	curr_state["_id"] = "STATE" + (Number(ts) + 1).toString();
    	}
		else 
    	{
    	curr_state["_id"] = "STATE" + Number(ts).toString();
    	}
    	*/
		//console.log(ServiceID, data.Root.Gateway);
		//curr_state["timestamp"] = Number(data.Nodes[0]['Sensors-Actuators'][0]['TimeStamp']);
		curr_state[data.Root.Gateway.toString() + "_NB_CONN"] = Number(data.Nodes[0]['Sensors-Actuators'][0].Resources['5700']);
		curr_state[data.Root.Gateway.toString() + "_NB_CONN_UP"] = Number(data.Nodes[0]['Sensors-Actuators'][1].Resources['5700']);
		curr_state[data.Root.Gateway.toString() + "_AVG_PUB_SIZE"] = Number(data.Nodes[0]['Sensors-Actuators'][2].Resources['5700']);
		curr_state[data.Root.Gateway.toString() + "_AVG_RECV_SIZE"] = Number(data.Nodes[0]['Sensors-Actuators'][3].Resources['5700']);
		curr_state[data.Root.Gateway.toString() + "_PUB_RATE"] = Number(data.Nodes[0]['Sensors-Actuators'][4].Resources['5700']);
		curr_state[data.Root.Gateway.toString() + "_RECV_RATE"] = Number(data.Nodes[0]['Sensors-Actuators'][5].Resources['5700']);
		curr_state[data.Root.Gateway.toString() + "_MS_DELAY"] = Number(data.Nodes[0]['Sensors-Actuators'][6].Resources['5700']);
		
		//gateway missing
		
		//gateway missing
		curr_state[gw_missing + "_NB_CONN"] = 0;
		curr_state[gw_missing + "_NB_CONN_UP"] = 0;
		curr_state[gw_missing + "_AVG_PUB_SIZE"] = 0;
		curr_state[gw_missing + "_AVG_RECV_SIZE"] = 0;
		curr_state[gw_missing + "_PUB_RATE"] = 0;
		curr_state[gw_missing + "_RECV_RATE"] = 0;
		curr_state[gw_missing + "_MS_DELAY"] = 0;
		
		
		if (Object.keys(curr_state).length == 123) {
			insert_to_raw_data_real_time(curr_state);
			report_to_INDRA_broker(gw, TimeStamp, TimeAccuracy);
		}
	}
	console.log(Object.keys(curr_state).length);
	console.log(curr_state["_id"]);
}
/*client.on('connect', function () {
  console.log('Connected');
});
*/
client.on('error', function (err) {
  console.error('[ERROR] Cannot connect to MQTT-BROKER\n', err);
});
