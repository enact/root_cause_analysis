# Root Cause Analysis

Root Cause Analysis Enabler

## Setup

```
cd root_cause_analysis/root_cause_analysis
npm install
```

## Usage

### Start application
```
npm run start
```
Access to the RCA Enabler dashboard at: `http://your_ip:31057`

_Customize dashboard address_

Create `.env` file: `cp env.example .env`
Update the `host` and `port` then start the application.

### Start application with `forever`
Install `forever`

```
npm install forever -g
```

Start RCA GUI 
```
npm run forever-start
```

Stop Test and Simulation
```
npm run forever-stop
