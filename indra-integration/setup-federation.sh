# Create Vhost
echo 'Creating the ENACT vhost'
sudo rabbitmqctl add_vhost ENACT

echo 'Creating a new user'
sudo rabbitmqctl add_user mmtbox Montimage2019

echo 'Add permission'
sudo rabbitmqctl set_permissions -p ENACT mmtbox ".*" ".*" ".*"
# enable
echo 'Enable federation plugin'
sudo rabbitmq-plugins enable rabbitmq_federation
sudo rabbitmq-plugins enable rabbitmq_federation_management

echo 'Setting up the federation upstream'
sudo rabbitmqctl set_parameter federation-upstream indra-enact-upstream '{"uri":"amqps://drivera:Enact2019@54.154.144.55:5671/ENACT?cacertfile=/home/montimage/certs/public/extca-chain.cert.pem&certfile=/home/montimage/certs/public/drivera.cert.pem&keyfile=/home/montimage/certs/private/drivera.key.pem", "exchange":"MONTIMAGE_E", "expires":3600000}'  -p ENACT
sudo rabbitmqctl list_parameters -p ENACT

# The local exchange will be created by receiver.py
# echo 'Creating a local exchange'
# python createExchange.py
# sudo rabbitmqctl list_exchanges -p ENACT

echo 'Define the policy'
sudo rabbitmqctl set_policy --apply-to exchanges indra-enact-policy "LOCAL_E" '{"federation-upstream-set":"all"}' -p ENACT
sudo rabbitmqctl list_policies -p ENACT

echo 'Check the federation status'
sudo rabbitmqctl eval 'rabbit_federation_status:status().'