#!/bin/bash
if [ "$1" = "status" ]
then
  rabbitmqctl eval 'rabbit_federation_status:status().'
elif [ "$1" = "policy" ]
then
  rabbitmqctl list_policies -p ENACT
elif [ "$1" = "exchange" ]
then
  rabbitmqctl list_exchanges -p ENACT
elif [ "$1" = "binding" ]
then
  rabbitmqctl list_bindings -p ENACT
elif [ "$1" = "parameter" ]
then
  rabbitmqctl list_parameters -p ENACT
elif [ "$1" = "auth" ]
then
  rabbitmqctl set_parameter federation-upstream indra-enact-upstream '{"uri":"amqps://'$2:$3'@54.154.144.55:5671/ENACT?cacertfile=/home/montimage/certs/public/extca-chain.cert.pem&certfile=/home/montimage/certs/public/drivera.cert.pem&keyfile=/home/montimage/certs/private/drivera.key.pem", "exchange":"MONTIMAGE_E", "expires":3600000}'  -p ENACT
else
  echo "Unsupported action: $1"
fi