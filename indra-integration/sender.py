#!/usr/bin/env python
import pika
from datetime import datetime

# CONFIGURATION
# rabbitHost = '192.168.0.174'
# rabbitPort = 5672
# rabbitVhost = 'enact'
# rabbitUsername = 'montimage_enact'
# rabbitPassword = '_MMT_enact_connect_2019'

# CONFIGURATION
rabbitHost = '192.168.0.174'
rabbitPort = 5672
rabbitVhost = 'ENACT'
rabbitUsername = 'mmtbox'
rabbitPassword = 'Montimage2019'


connURL = 'amqp://' + rabbitUsername + ':' + rabbitPassword + '@' + rabbitHost + ':' + str(rabbitPort) + '/' + rabbitVhost

connection = pika.BlockingConnection(pika.URLParameters(connURL))
# Create channel
channel = connection.channel()
channel.exchange_declare(exchange='LOCAL_E', exchange_type='fanout', durable=True)

# Send a message
msgBody = 'Hello ENACT - '  + str(datetime.now())
channel.basic_publish(exchange='LOCAL_E', routing_key='', body=msgBody)
print("[x] Sent " + msgBody)
connection.close()