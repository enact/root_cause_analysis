import socket
from datetime import datetime

socketServerDomain = '10.0.0.11'
socketServerPort = 1001

# create a TCP/IP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# bind the socket to the port
serverAddress = (socketServerDomain, socketServerPort)
print('[SOCKET] Start the socket server: ' + str(serverAddress))
sock.bind(serverAddress)

# Listen for incoming connections
sock.listen(1)

while True:
  # Wait for a connection
  print('[SOCKET] Waiting for a connection ...')
  connection, clientAddress = sock.accept()

  try:
    print('[SOCKET] Connection from' + str(clientAddress))

    # Receive the data in small chunks and print it
    while True:
      data = connection.recv(2048)
      if data:
        print('[MMT-PROBE]: received new message at timestamp: ', datetime.now())
        print(data.decode())
        print('\n')
        # print('no more data from' + str(clientAddress))
        # connection.close()
  finally:
    # clean up the connection
    connection.close()