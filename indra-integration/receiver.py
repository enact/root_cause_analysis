#!/usr/bin/env python
import pika
import socket
from datetime import datetime

# # # # # # # # # #
# CONFIGURATIONS
# # # # # # # # # #
socketServerDomain = '10.0.0.11' # Domain name of socket server
socketServerPort = 1001 # Port number of socket server
# rabbitHost = '192.168.0.174'
# rabbitPort = 5672
# rabbitVhost = 'enact'
# rabbitUsername = 'montimage_enact'
# rabbitPassword = '_MMT_enact_connect_2019'

rabbitHost = '192.168.0.220'
rabbitPort = 5672
rabbitVhost = 'ENACT'
rabbitUsername = 'mmtbox'
rabbitPassword = 'Montimage2019'
rabbitLocalExchange='LOCAL_E'

# # # # # # # # # #
# SOCKET FUNCTIONS
# # # # # # # # # #

# create a TCP/IP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# Initialize socket
def initSocket():
  # bind the socket to the port
  serverAddress = (socketServerDomain, socketServerPort)
  print('[SOCKET-CLIENT] Connecting to socket server %s:%s' % serverAddress)
  sock.connect(serverAddress)

def sendMessageToSocket(message):
  # print('Going to send data to socket: ' + message)
  sock.sendall(message)
  print('[SOCKET-CLIENT] A message has been sent to socket!')

# # # # # # # # # #
# RABBITMQ FUNCTIONS
# # # # # # # # # #

def onMessage(channel, method, properties, message):
  print("[RECEIVER] Received a message at timestamp: ", datetime.now())
  sendMessageToSocket(message)

def initRabbitMQ():
  connURL = 'amqp://' + rabbitUsername + ':' + rabbitPassword + '@' + rabbitHost + ':' + str(rabbitPort) + '/' + rabbitVhost
  connection = pika.BlockingConnection(pika.URLParameters(connURL))
  # Create channel
  channel = connection.channel()
  # Declare the local exchange
  channel.exchange_declare(exchange=rabbitLocalExchange, exchange_type='fanout', durable=True)
  # Declare the queue
  result = channel.queue_declare(queue='', exclusive=True)
  queue_name = result.method.queue
  # Bind the local exchange with the generated queue
  channel.queue_bind(exchange=rabbitLocalExchange, queue=queue_name, routing_key='#')
  print('[RECEIVER] Waiting for messages on queue %s' % queue_name)
  print('To exit press CTRL+C ...')
  channel.basic_consume(queue=queue_name,
                        auto_ack=True,
                        on_message_callback=onMessage)
  channel.start_consuming()

# # # # # # # # # #
# START APPLICATION
# # # # # # # # # #
initSocket()
initRabbitMQ()