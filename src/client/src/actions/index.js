import {createAction } from 'redux-act';
// Notification
export const setNotification = createAction('SET_NOTIFICATION');
export const resetNotification = createAction('RESET_NOTIFICATION');

// KnownIncident
export const requestAllKnownIncidents = createAction('REQUEST_ALL_KNOWN_INCIDENTS');
export const setAllKnownIncidents = createAction('SET_ALL_KNOWN_INCIDENTS');
export const setCurrentKnownIncident = createAction('SET_CURRENT_KNOWN_INCIDENT');
export const requestDeleteKnownIncident = createAction('REQUEST_DELETE_KNOWN_INCIDENT');
export const deleteKnownIncidentOK = createAction('DELETE_KNOWN_INCIDENT_OK');
export const requestAddNewKnownIncident = createAction('REQUEST_ADD_NEW_KNOWN_INCIDENT');
export const addNewKnownIncidentOK = createAction('ADD_NEW_KNOWN_INCIDENT_OK');
export const requestKnownIncident = createAction('REQUEST_KNOWN_INCIDENT');
export const requestUpdateKnownIncident = createAction('REQUEST_UPDATE_KNOWN_INCIDENT');
export const updateKnownIncidentOK = createAction('UPDATE_KNOWN_INCIDENT_OK');

// NewIncident
export const requestAllNewIncidents = createAction('REQUEST_ALL_NEW_INCIDENTS');
export const requestAllKnownIncidentsByNewIncidentId = createAction('REQUEST_ALL_KNOWN_INCIDENTS_BY_NEW_INCIDENT_ID');
export const setAllNewIncidents = createAction('SET_ALL_NEW_INCIDENTS');
export const setCurrentNewIncident = createAction('SET_CURRENT_NEW_INCIDENT');
export const setKnownIncidents = createAction('SET_KNOWN_INCIDENTS');
export const requestDeleteNewIncident = createAction('REQUEST_DELETE_NEW_INCIDENT');
export const deleteNewIncidentOK = createAction('DELETE_NEW_INCIDENT_OK');
export const requestAddNewNewIncident = createAction('REQUEST_ADD_NEW_NEW_INCIDENT');
export const addNewNewIncidentOK = createAction('ADD_NEW_NEW_INCIDENT_OK');
export const requestNewIncident = createAction('REQUEST_NEW_INCIDENT');
export const requestUpdateNewIncident = createAction('REQUEST_UPDATE_NEW_INCIDENT');
export const updateNewIncidentOK = createAction('UPDATE_NEW_INCIDENT_OK');