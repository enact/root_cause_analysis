// read and pass the environment variables into reactjs application
// const URL = `http://localhost:9112`;
const URL = "";

// Known Incident
export const sendRequestKnownIncident = async (tcId) => {
  const url = `${URL}/api/known-incidents/${tcId}`;
  const response = await fetch(url);
  const status = await response.json();
  if (status.error) {
    throw status.error;
  }
  return status.knownIncident;
};

export const sendRequestUpdateKnownIncident = async (id, knownIncident) => {
  const url = `${URL}/api/known-incidents/${id}`;
  const response = await fetch(url,{
    method: 'POST',
    headers: {
      'Content-Type':'application/json'
    },
    body: JSON.stringify({knownIncident})
  });
  const data = await response.json();
  if (data.error) {
    throw data.error;
  }
  return data.knownIncident;
};

export const sendRequestAllKnownIncidents = async () => {
  const url = `${URL}/api/known-incidents`;
  const response = await fetch(url);
  const data = await response.json();
  if (data.error) {
    throw data.error;
  }
  return data.knownIncidents;
};

export const sendRequestAddNewKnownIncident = async (knownIncident) => {
  const url = `${URL}/api/known-incidents`;
  const response = await fetch(url,{
    method: 'POST',
    headers: {
      'Content-Type':'application/json'
    },
    body: JSON.stringify({knownIncident})
  });
  const data = await response.json();
  if (data.error) {
    throw data.error;
  }
  return data.knownIncident;
};

export const sendRequestDeleteKnownIncident = async (knownIncidentId) => {
  const url = `${URL}/api/known-incidents/${knownIncidentId}`;
  const response = await fetch(url,{
    method: 'DELETE',
  });
  const data = await response.json();
  if (data.error) {
    throw data.error;
  }
  return data.result;
};



// new Incident
export const sendRequestNewIncident = async (tcId) => {
  const url = `${URL}/api/new-incidents/${tcId}`;
  const response = await fetch(url);
  const status = await response.json();
  if (status.error) {
    throw status.error;
  }
  return status.newIncident;
};

export const sendRequestUpdateNewIncident = async (id, newIncident) => {
  const url = `${URL}/api/new-incidents/${id}`;
  const response = await fetch(url,{
    method: 'POST',
    headers: {
      'Content-Type':'application/json'
    },
    body: JSON.stringify({newIncident})
  });
  const data = await response.json();
  if (data.error) {
    throw data.error;
  }
  return data.newIncident;
};

export const sendRequestAllKnownIncidentsByNewIncidentId = async (newIncidentId) => {
  const url = `${URL}/api/new-incidents/${newIncidentId}`;
  const response = await fetch(url);
  const data = await response.json();
  if (data.error) {
    throw data.error;
  }
  const {knownIncidents, similarity_score} = data;
  const incidents = knownIncidents.map(i => ({...i, score: similarity_score[i._id]}));
  return incidents;
};


export const sendRequestAllNewIncidents = async () => {
  const url = `${URL}/api/new-incidents`;
  const response = await fetch(url);
  const data = await response.json();
  if (data.error) {
    throw data.error;
  }
  return data.newIncidents;
};

export const sendRequestAddNewNewIncident = async (newIncident) => {
  const url = `${URL}/api/new-incidents`;
  const response = await fetch(url,{
    method: 'POST',
    headers: {
      'Content-Type':'application/json'
    },
    body: JSON.stringify({newIncident})
  });
  const data = await response.json();
  if (data.error) {
    throw data.error;
  }
  return data.newIncident;
};

export const sendRequestDeleteNewIncident = async (newIncidentId) => {
  const url = `${URL}/api/new-incidents/${newIncidentId}`;
  const response = await fetch(url,{
    method: 'DELETE',
  });
  const data = await response.json();
  if (data.error) {
    throw data.error;
  }
  return data.result;
};

