import {
  combineReducers
} from 'redux';

import knownIncidentsReducer from './knownIncidentsReducer';
import newIncidentsReducer from './newIncidentsReducer';
import notificationReducer from './notificationReducer';

const rootReducer = combineReducers({
  knownIncidents: knownIncidentsReducer,
  newIncidents: newIncidentsReducer,
  notify: notificationReducer,
});

export default rootReducer;
