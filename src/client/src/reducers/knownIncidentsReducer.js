import {
  createReducer
} from "redux-act";
import produce from "immer";
import {
  setAllKnownIncidents,
  addNewKnownIncidentOK,
  deleteKnownIncidentOK
} from "../actions";

import { addNewElementToArray, removeElementFromArray } from "../utils";

const initState = [];

export default createReducer({
    [setAllKnownIncidents]: (state, knownIncidents) => knownIncidents,
    [addNewKnownIncidentOK]: produce((draft, newNI) => {
      if (draft) {
        const newKnownIncidents = addNewElementToArray(draft, newNI);
        draft = [...newKnownIncidents];
      } else {
        draft = [newNI];
      }
    }),
    [deleteKnownIncidentOK]: produce((draft, knownIncidentId) => {
      const newKnownIncidents = removeElementFromArray(draft, knownIncidentId);
      if (newKnownIncidents) draft = [...newKnownIncidents];
    })
  },
  initState
);