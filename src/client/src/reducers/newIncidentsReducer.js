import {
  createReducer
} from "redux-act";
import produce from "immer";
import {
  setCurrentNewIncident,
  setAllNewIncidents,
  addNewNewIncidentOK,
  deleteNewIncidentOK,
  setKnownIncidents
} from "../actions";

import { addNewElementToArray, removeElementFromArray } from "../utils";

const initState = {
  newIncidents: [],
  currentNewIncident: {
    newIncident: null,
    knownIncidents:[]
  }
};

export default createReducer({
    [setAllNewIncidents]: produce((draft, newIncidents) => {
      draft.newIncidents = newIncidents;
    }),
    [addNewNewIncidentOK]: produce((draft, newNI) => {
      if (draft.newIncidents) {
        const newNewIncidents = addNewElementToArray(draft.newIncidents, newNI);
        draft.newIncidents = [...newNewIncidents];
      } else {
        draft.newIncidents = [newNI];
      }
      draft.currentNewIncident.newIncident = newNI;
    }),
    [deleteNewIncidentOK]: produce((draft, newIncidentId) => {
      const newNewIncidents = removeElementFromArray(draft.newIncidents, newIncidentId);
      if (newNewIncidents) draft.newIncidents = [...newNewIncidents];
    }),
    [setCurrentNewIncident]: produce((draft, newIncident) => {
      draft.currentNewIncident.newIncident = newIncident;
    }),
    [setKnownIncidents]: produce((draft, knownIncidents) => {
      draft.currentNewIncident.knownIncidents = knownIncidents;
    }),
  },
  initState
);