import React, { Component } from "react";
import { connect } from "react-redux";
import { Table, Popover, Button } from "antd";
import LayoutPage from "./LayoutPage";
import {
  requestAllNewIncidents,
} from "../actions";

class NewIncidentListPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      attributeListId: null,
    }
  }

  componentDidMount() {
    this.props.fetchNewIncidents();
  }

  render() {
    const { newIncidents } = this.props;
    const dataSource = newIncidents.map((ni) => ({ ...ni, key: ni._id }));
    const columns = [
      {
        title: "Id",
        key: "data",
        render: (ni) => ni._id,
      },
      {
        title: "Timestamp",
        key: "data",
        render: (ni) => ni.timestamp,
      },
      {
        title: "Similar Known Incidents",
        key: "data",
        render: (ni) => <a href={`/new-incidents/${ni._id}`}> View Similarity Incidents </a>,
      },
      {
        title: "Attributes",
        key: "data",
        render: (ni) => {
          let attributeEntries = Object.entries(ni.attributes);
          attributeEntries.sort((a, b) => {
            // console.log(a[0], b[0]);
            return Number(a[0].replace('attribute_','')) - Number(b[0].replace('attribute_',''));
          });
          let attributeDataSource = attributeEntries.map((a, index) => ({
            name: a[0],
            value: a[1],
            key: index,
          }));
          const attColumns = [
            {
              title: "Index",
              dataIndex: "key",
              key: "key",
            },
            {
              title: "Name",
              dataIndex: "name",
              key: "name",
            },
            {
              title: "Value",
              dataIndex: "value",
              key: "value",
            },
          ];
          return (
            <Popover
              content={
                <div>
                <Button onClick={() => this.setState({ attributeListId: null })} danger style={{margin: 10}}>
                    Close
                  </Button>  
                <Table
                    dataSource={attributeDataSource}
                    columns={attColumns}
                  />
                </div>
              }
              title="Attributes"
              trigger="click"
              visible={this.state.attributeListId === ni._id}
              onVisibleChange={() => {
                if (this.state.attributeListId) {
                  this.setState({ attributeListId: null });
                } else {
                  this.setState({ attributeListId: ni._id });
                }
              }}
            >
              <Button type="primary">View</Button>
            </Popover>
          );
        },
      },
    ];
    return (
      <LayoutPage
        pageTitle="New Incidents"
        pageSubTitle="Recent detected incidents"
      >
        <Table columns={columns} dataSource={dataSource} />
      </LayoutPage>
    );
  }
}

const mapPropsToStates = ({ newIncidents }) => ({
  newIncidents: newIncidents.newIncidents
});

const mapDispatchToProps = (dispatch) => ({
  fetchNewIncidents: () => dispatch(requestAllNewIncidents())
});

export default connect(
  mapPropsToStates,
  mapDispatchToProps
)(NewIncidentListPage);
