import React, { Component } from "react";
import { connect } from "react-redux";
import { notification, Layout, Typography } from "antd";
import { resetNotification } from "../actions";
import TSFooter from "../components/TSFooter";
import "./styles.css";
const { Title, Text } = Typography;

const { Content } = Layout;

class LayoutPage extends Component {
  render() {
    const { notify, resetNotification, pageTitle, pageSubTitle } = this.props;
    return (
      <Layout>
        {notify &&
          notification[notify.type]({
            message: notify.type.toUpperCase(),
            description:
              typeof notify.message === "object"
                ? JSON.stringify(notify.message)
                : notify.message,
            onClose: () => resetNotification(),
          })}
        <Layout style={{ padding: "0px 48px 48px", margin: "30px 50px 50px" }}>
          <Content>
            {pageTitle && <Title level={2}>{pageTitle}</Title>}
            {pageSubTitle && <Text type="secondary">{pageSubTitle}</Text>}
            <div style={{ paddingTop: "30px" }} className="site-layout-content">
              {this.props.children}
              <TSFooter />
            </div>
          </Content>
        </Layout>
      </Layout>
    );
  }
}

const mapPropsToStates = ({ notify }) => ({
  notify,
});

const mapDispatchToProps = (dispatch) => ({
  resetNotification: () => dispatch(resetNotification()),
});

export default connect(mapPropsToStates, mapDispatchToProps)(LayoutPage);
