import React, { Component } from "react";
import { connect } from "react-redux";
import { Table, Popover, Button } from "antd";
import LayoutPage from "./LayoutPage";
import { requestAllKnownIncidents } from "../actions";

class KnownIncidentListPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      attributeListId: null,
    };
  }

  componentDidMount() {
    this.props.fetchknownIncidents();
  }

  render() {
    const { knownIncidents } = this.props;
    const dataSource = knownIncidents.map((ni) => ({ ...ni, key: ni._id }));
    const columns = [
      {
        title: "Id",
        key: "data",
        render: (ni) => ni._id,
      },
      {
        title: "Timestamp",
        key: "data",
        render: (ni) => ni.timestamp,
      },
      {
        title: "Description",
        key: "data",
        render: (ni) => ni.description,
      },
      {
        title: "Attributes",
        key: "data",
        render: (ni) => {
          let attributeEntries = Object.entries(ni.attributes);
          attributeEntries.sort((a, b) => {
            // console.log(a[0], b[0]);
            return Number(a[0].replace('attribute_','')) - Number(b[0].replace('attribute_',''));
          });
          let attributeDataSource = attributeEntries.map((a, index) => ({
            name: a[0],
            value: a[1],
            key: index,
          }));
          const attColumns = [
            {
              title: "Index",
              dataIndex: "key",
              key: "key",
            },
            {
              title: "Name",
              dataIndex: "name",
              key: "name",
            },
            {
              title: "Value",
              dataIndex: "value",
              key: "value",
            },
          ];
          return (
            <Popover
              content={
                <div>
                <Button onClick={() => this.setState({ attributeListId: null })} danger style={{margin: 10}}>
                    Close
                  </Button>  
                <Table
                    dataSource={attributeDataSource}
                    columns={attColumns}
                  />
                </div>
              }
              title="Attributes"
              trigger="click"
              visible={this.state.attributeListId === ni._id}
              onVisibleChange={() => {
                if (this.state.attributeListId) {
                  this.setState({ attributeListId: null });
                } else {
                  this.setState({ attributeListId: ni._id });
                }
              }}
            >
              <Button type="primary">View</Button>
            </Popover>
          );
        },
      },
    ];
    return (
      <LayoutPage
        pageTitle="Known Incident"
        pageSubTitle="All the known incidents"
      >
        <Table columns={columns} dataSource={dataSource} />
      </LayoutPage>
    );
  }
}

const mapPropsToStates = ({ knownIncidents }) => ({
  knownIncidents: knownIncidents,
});

const mapDispatchToProps = (dispatch) => ({
  fetchknownIncidents: () => dispatch(requestAllKnownIncidents()),
});

export default connect(
  mapPropsToStates,
  mapDispatchToProps
)(KnownIncidentListPage);
