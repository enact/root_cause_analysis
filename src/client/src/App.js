import React from "react";
import "antd/dist/antd.css";
import { Layout } from "antd";
import { BrowserRouter as Router, Switch, Route, Redirect } from "react-router-dom";

import ErrorBoundary from "antd/lib/alert/ErrorBoundary";
import TSHeader from "./components/TSHeader";
import NewIncidentListPage from "./pages/NewIncidentListPage";
import KnownIncidentListPage from "./pages/KnownIncidentListPage";
import SimilarityKnownIncidentListPage from "./pages/SimilarityKnownIncidentListPage";

function App() {
  return (
    <Router>
      <ErrorBoundary>
        <Layout className="layout" style={{ height: "100%" }}>
          <TSHeader />
          <Switch>
            <Route exact path="/" render={() => <Redirect to="/new-incidents"/>} />
            <Route path="/new-incidents/:newIncidentId">
              <SimilarityKnownIncidentListPage />
            </Route>
            <Route path="/new-incidents">
              <NewIncidentListPage/>
            </Route>
            <Route path="/known-incidents">
              <KnownIncidentListPage/>
            </Route>
          </Switch>          
        </Layout>
      </ErrorBoundary>
    </Router>
  );
}

export default App;
