// watcher saga -> actions -> worker saga
import {
  call,
  put,
  takeEvery,
} from 'redux-saga/effects';

import {
  sendRequestAllNewIncidents,
  sendRequestAddNewNewIncident,
  sendRequestDeleteNewIncident,
  sendRequestNewIncident,
  sendRequestUpdateNewIncident,
  sendRequestAllKnownIncidentsByNewIncidentId,
} from '../api';
import {
  setNotification,
  setAllNewIncidents,
  addNewNewIncidentOK,
  deleteNewIncidentOK,
  setCurrentNewIncident,
  setKnownIncidents,
} from '../actions';

function* handleRequestNewIncident(action) {
  try {
    const newIncidentId = action.payload;
    const newIncident = yield call(() => sendRequestNewIncident(newIncidentId));
    yield put(setCurrentNewIncident(newIncident));
    // dispatch data
  } catch (error) {
    // dispatch error
    yield put(setNotification({
      type: 'error',
      message: error
    }));
  }
}

function* handleRequestUpdateNewIncident(action) {
  try {
    const {id, newIncident } = action.payload;
    const newNewIncident = yield call(() => sendRequestUpdateNewIncident(id, newIncident));
    yield put(addNewNewIncidentOK(newNewIncident));
    yield put(setNotification({
      type: 'success',
      message: `A new NewIncident ${newNewIncident._id} has been updated`
    }));
    // dispatch data
  } catch (error) {
    // dispatch error
    yield put(setNotification({
      type: 'error',
      message: error
    }));
  }
}


function* handleRequestAllKnownIncidentByNewIncidentId(action) {
  try {
    const newIncidentId = action.payload;
    const knownIncidents = yield call(() => sendRequestAllKnownIncidentsByNewIncidentId(newIncidentId));
    yield put(setKnownIncidents(knownIncidents));
    // dispatch data
  } catch (error) {
    // dispatch error
    yield put(setNotification({
      type: 'error',
      message: error
    }));
  }
}

function* handleRequestAllNewIncidents() {
  try {
    const allNewIncidents = yield call(() => sendRequestAllNewIncidents());
    yield put(setAllNewIncidents(allNewIncidents));
    // dispatch data
  } catch (error) {
    // dispatch error
    yield put(setNotification({
      type: 'error',
      message: error
    }));
  }
}

function* handleRequestAddNewNewIncident(action) {
  try {
    const newIncident = action.payload;
    const newNewIncident = yield call(() => sendRequestAddNewNewIncident(newIncident));
    yield put(addNewNewIncidentOK(newNewIncident));
    yield put(setNotification({
      type: 'success',
      message: `A new NewIncident ${newNewIncident._id} has been added`
    }));
    // dispatch data
  } catch (error) {
    // dispatch error
    yield put(setNotification({
      type: 'error',
      message: error
    }));
  }
}

function* handleRequestDeleteNewIncident(action) {
  try {
    const newIncidentId = action.payload;
    yield call(() => sendRequestDeleteNewIncident(newIncidentId));
    yield put(deleteNewIncidentOK(newIncidentId));
    yield put(setNotification({
      type: 'success',
      message: `Test case ${newIncidentId} has been deleted`
    }));
    // dispatch data
  } catch (error) {
    // dispatch error
    yield put(setNotification({
      type: 'error',
      message: error
    }));
  }
}

function* watchNewIncidents() {
  yield takeEvery('REQUEST_NEW_INCIDENT', handleRequestNewIncident);
  yield takeEvery('REQUEST_UPDATE_NEW_INCIDENT', handleRequestUpdateNewIncident);
  yield takeEvery('REQUEST_ALL_KNOWN_INCIDENTS_BY_NEW_INCIDENT_ID', handleRequestAllKnownIncidentByNewIncidentId);
  yield takeEvery('REQUEST_ALL_NEW_INCIDENTS', handleRequestAllNewIncidents);
  yield takeEvery('REQUEST_ADD_NEW_NEW_INCIDENT', handleRequestAddNewNewIncident);
  yield takeEvery('REQUEST_DELETE_NEW_INCIDENT', handleRequestDeleteNewIncident);
}

export default watchNewIncidents;