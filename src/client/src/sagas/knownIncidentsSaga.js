// watcher saga -> actions -> worker saga
import {
  call,
  put,
  takeEvery,
} from 'redux-saga/effects';

import {
  sendRequestAllKnownIncidents,
  sendRequestAddNewKnownIncident,
  sendRequestDeleteKnownIncident,
  sendRequestKnownIncident,
  sendRequestUpdateKnownIncident
} from '../api';
import {
  setNotification,
  setAllKnownIncidents,
  addNewKnownIncidentOK,
  deleteKnownIncidentOK,
  setCurrentKnownIncident,
} from '../actions';

function* handleRequestKnownIncident(action) {
  try {
    const knownIncidentId = action.payload;
    const knownIncident = yield call(() => sendRequestKnownIncident(knownIncidentId));
    yield put(setCurrentKnownIncident(knownIncident));
    // dispatch data
  } catch (error) {
    // dispatch error
    yield put(setNotification({
      type: 'error',
      message: error
    }));
  }
}

function* handleRequestUpdateKnownIncident(action) {
  try {
    const {id, knownIncident } = action.payload;
    const newKnownIncident = yield call(() => sendRequestUpdateKnownIncident(id, knownIncident));
    yield put(addNewKnownIncidentOK(newKnownIncident));
    yield put(setNotification({
      type: 'success',
      message: `A new KnownIncident ${newKnownIncident._id} has been updated`
    }));
    // dispatch data
  } catch (error) {
    // dispatch error
    yield put(setNotification({
      type: 'error',
      message: error
    }));
  }
}


function* handleRequestAllKnownIncidents() {
  try {
    const allKnownIncidents = yield call(() => sendRequestAllKnownIncidents());
    yield put(setAllKnownIncidents(allKnownIncidents));
    // dispatch data
  } catch (error) {
    // dispatch error
    yield put(setNotification({
      type: 'error',
      message: error
    }));
  }
}

function* handleRequestAddNewKnownIncident(action) {
  try {
    const knownIncident = action.payload;
    const newKnownIncident = yield call(() => sendRequestAddNewKnownIncident(knownIncident));
    yield put(addNewKnownIncidentOK(newKnownIncident));
    yield put(setNotification({
      type: 'success',
      message: `A new KnownIncident ${newKnownIncident._id} has been added`
    }));
    // dispatch data
  } catch (error) {
    // dispatch error
    yield put(setNotification({
      type: 'error',
      message: error
    }));
  }
}

function* handleRequestDeleteKnownIncident(action) {
  try {
    const knownIncidentId = action.payload;
    yield call(() => sendRequestDeleteKnownIncident(knownIncidentId));
    yield put(deleteKnownIncidentOK(knownIncidentId));
    yield put(setNotification({
      type: 'success',
      message: `Test case ${knownIncidentId} has been deleted`
    }));
    // dispatch data
  } catch (error) {
    // dispatch error
    yield put(setNotification({
      type: 'error',
      message: error
    }));
  }
}

function* watchKnownIncidents() {
  yield takeEvery('REQUEST_KNOWN_INCIDENT', handleRequestKnownIncident);
  yield takeEvery('REQUEST_UPDATE_KNOWN_INCIDENT', handleRequestUpdateKnownIncident);
  yield takeEvery('REQUEST_ALL_KNOWN_INCIDENTS', handleRequestAllKnownIncidents);
  yield takeEvery('REQUEST_ADD_NEW_KNOWN_INCIDENT', handleRequestAddNewKnownIncident);
  yield takeEvery('REQUEST_DELETE_KNOWN_INCIDENT', handleRequestDeleteKnownIncident);
}

export default watchKnownIncidents;