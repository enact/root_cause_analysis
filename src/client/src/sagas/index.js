import { all } from "redux-saga/effects";

import knownIncidentsSaga from './knownIncidentsSaga';
import newIncidentsSaga from './newIncidentsSaga';

function* rootSaga() {
  yield all([
    newIncidentsSaga(),
    knownIncidentsSaga(),
  ]);
}

export default rootSaga;
