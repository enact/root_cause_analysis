/**
 * - id: String
- name: String
- description: String
- tags: [String] # array of tags
- events: [Event]

 */

const mongoose = require("mongoose");

const Schema = mongoose.Schema;
/**
 * TODO: 
 * - get NewIncident by name/id
 * - get NewIncident by tags
 */
const newIncidentSchema = new Schema({
  _id: {
    type: String,
    required: true
  },
  attributes: {
    type: Object,
    required: true
  },
  similarity_score: {
    type: Object,
    required: true
  },
  timestamp: {
    type: String,
    required: true
  }
});

newIncidentSchema.statics.findNewIncidentsWithPagingOptions = function (options, page, callback) {
  this.find(options)
    .limit(20)
    .skip(page * 20)
    .sort({
      timestamp: 1
    })
    .exec((err, data) => {
      if (err) {
        return callback(err);
      }

      if (!data) {
        return callback({
          error: `Cannot find any event data`
        });
      }

      return callback(null, data);
    });
};

newIncidentSchema.statics.findNewIncidentsWithOptions = function (options, callback) {
  this.find(options)
    .sort({
      timestamp: 1
    })
    .exec((err, data) => {
      if (err) {
        return callback(err);
      }

      if (!data) {
        return callback({
          error: `Cannot find any recorded data`
        });
      }

      return callback(null, data);
    });
};

newIncidentSchema.set('collection','data_real_time');

module.exports = mongoose.model("NewIncident", newIncidentSchema);