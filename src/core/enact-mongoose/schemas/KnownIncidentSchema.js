/**
 * - id: String
- name: String
- description: String
- tags: [String] # array of tags
- events: [Event]

 */

const mongoose = require("mongoose");

const Schema = mongoose.Schema;
/**
 * TODO: 
 * - get KnownIncident by name/id
 * - get KnownIncident by tags
 */
const knownIncidentSchema = new Schema({
  _id: {
    type: String,
    required: true
  },
  attributes: {
    type: Object,
    required: true
  },
  description: {
    type: String,
    required: false
  },
  timestamp: {
    type: String,
    required: true
  }
});

knownIncidentSchema.statics.findKnownIncidentsWithPagingOptions = function (options, page, callback) {
  this.find(options)
    .limit(20)
    .skip(page * 20)
    .sort({
      timestamp: 1
    })
    .exec((err, data) => {
      if (err) {
        return callback(err);
      }

      if (!data) {
        return callback({
          error: `Cannot find any event data`
        });
      }

      return callback(null, data);
    });
};

knownIncidentSchema.statics.findKnownIncidentsWithOptions = function (options, callback) {
  this.find(options)
    .sort({
      timestamp: 1
    })
    .exec((err, data) => {
      if (err) {
        return callback(err);
      }

      if (!data) {
        return callback({
          error: `Cannot find any recorded data`
        });
      }

      return callback(null, data);
    });
};

knownIncidentSchema.set('collection','data_knowledge');

module.exports = mongoose.model("KnownIncident", knownIncidentSchema);