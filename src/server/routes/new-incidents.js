/* Working with known incident */
const express = require("express");
const router = express.Router();
const { NewIncidentSchema, dbConnector } = require("./db-connector");
const KnownIncidentSchema = require("../../core/enact-mongoose/schemas/KnownIncidentSchema");

router.get("/", dbConnector, function (req, res, next) {
  let page = req.query.page;
  if (!page) page = 0;
  NewIncidentSchema.findNewIncidentsWithPagingOptions(
    null,
    page,
    (err2, newIncidents) => {
      if (err2) {
        console.error("[SERVER] Failed to get newIncidents", err2);
        res.send({
          error: "Failed to get known incident",
        });
      } else {
        res.send({
          newIncidents,
        });
      }
    }
  );
});

/**
 * Get a known incidents in the similarity score of a new incidents
 */
router.get("/:newIncidentId", dbConnector, function (req, res, next) {
  const { newIncidentId } = req.params;

  NewIncidentSchema.findById(newIncidentId, (err2, newIncident) => {
    if (err2) {
      console.error("[SERVER] Failed to get newIncidents", err2);
      res.send({
        error: "Failed to get new incident",
      });
    } else {
      const { similarity_score } = newIncident;
      if (!similarity_score) {
        res.send({
          error: "Failed to get the similarity_score",
        });
      } else {
        let knownIncidentEntries = Object.entries(similarity_score);
        // need to sort list of incident by value score
        knownIncidentEntries.sort((a, b) => {
          return a[1] - b[1];
        });
        const knownIncidentIds = knownIncidentEntries.map((a) => a[0]);
        KnownIncidentSchema.find()
          .where("_id")
          .in(knownIncidentIds)
          .exec((err, knownIncidents) => {
            if (err) {
              res.send({
                error: "Failed to get the list of known incidents",
              });
            } else {
              res.send({ knownIncidents, similarity_score });
            }
          });
      }
    }
  });
});

// Add a new known incident
router.post("/", dbConnector, function (req, res, next) {
  const { newIncident } = req.body;
  const { id, name, tags, description, source } = newIncident;
  const currentTime = Date.now();
  const newNewIncident = new NewIncidentSchema({
    id,
    name,
    tags,
    description,
    source,
    createdAt: currentTime,
    lastModified: currentTime,
  });
  newNewIncident.save((err, _newIncident) => {
    if (err) {
      console.error("[SERVER] Failed to save the known incidents", err);
      res.send({
        error: "Failed to save the known incident",
      });
    } else {
      res.send({
        newIncident: _newIncident,
      });
    }
  });
});

/**
 * Update a known incident
 */
router.post("/:newIncidentId", dbConnector, function (req, res, next) {
  const { newIncident } = req.body;
  const { newIncidentId } = req.params;

  NewIncidentSchema.findByIdAndUpdate(
    newIncidentId,
    { ...newIncident, timestamp: Date.now() },
    (err, ts) => {
      if (err) {
        console.error("[SERVER] Failed to save the known incidents", err);
        res.send({
          error: "Failed to save the known incident",
        });
      } else {
        res.send({
          newIncident: ts,
        });
      }
    }
  );
});

/**
 * Delete a known incident by id
 */
router.delete("/:newIncidentId", dbConnector, function (req, res, next) {
  const { newIncidentId } = req.params;

  NewIncidentSchema.findByIdAndDelete(newIncidentId, (err, ret) => {
    if (err) {
      console.error("[SERVER] Failed to save the known incidents", err);
      res.send({
        error: "Failed to save the known incident",
      });
    } else {
      res.send({
        result: ret,
      });
    }
  });
});

module.exports = router;
