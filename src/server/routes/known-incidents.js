/* Working with known incident */
const express = require("express");
const router = express.Router();
const {
  KnownIncidentSchema,
  dbConnector
} = require('./db-connector');

router.get("/", dbConnector, function (req, res, next) {
  let page = req.query.page;
  if (!page) page = 0;
  KnownIncidentSchema.findKnownIncidentsWithPagingOptions(null, page, (err2, knownIncidents) => {
    if (err2) {
      console.error('[SERVER] Failed to get knownIncidents', err2);
      res.send({
        error: 'Failed to get known incident'
      });
    } else {
      res.send({
        knownIncidents
      });
    }
  });
});

/**
 * Get a known incident by id
 */
router.get("/:knownIncidentId", dbConnector, function (req, res, next) {
  const {
    knownIncidentId
  } = req.params;

  KnownIncidentSchema.findByIdAndDelete(knownIncidentId, (err2, knownIncident) => {
    if (err2) {
      console.error('[SERVER] Failed to get knownIncidents', err2);
      res.send({
        error: 'Failed to get known incident'
      });
    } else {
      res.send({
        knownIncident
      });
    }
  });
});

// Add a new known incident
router.post("/", dbConnector, function (req, res, next) {
  const {
    knownIncident
  } = req.body;
  const {
    id,
    name,
    tags,
    description,
    source,
  } = knownIncident;
  const currentTime = Date.now();
  const newKnownIncident = new KnownIncidentSchema({
    id,
    name,
    tags,
    description,
    source,
    createdAt: currentTime,
    lastModified: currentTime
  });
  newKnownIncident.save((err, _knownIncident) => {
    if (err) {
      console.error('[SERVER] Failed to save the known incidents', err);
      res.send({
        error: 'Failed to save the known incident'
      });
    } else {
      res.send({
        knownIncident: _knownIncident
      });
    }
  });
});

/**
 * Update a known incident
 */
router.post("/:knownIncidentId", dbConnector, function (req, res, next) {
  const {
    knownIncident
  } = req.body;
  const {
    knownIncidentId
  } = req.params;

  KnownIncidentSchema.findByIdAndUpdate(knownIncidentId, {...knownIncident, timestamp: Date.now()}, (err, ts) => {
    if (err) {
      console.error('[SERVER] Failed to save the known incidents', err);
      res.send({
        error: 'Failed to save the known incident'
      });
    } else {
      res.send({
        knownIncident: ts
      });
    }
  });
});

/**
 * Delete a known incident by id
 */
router.delete("/:knownIncidentId", dbConnector, function (req, res, next) {
  const {
    knownIncidentId
  } = req.params;

  KnownIncidentSchema.findByIdAndDelete(knownIncidentId, (err, ret) => {
    if (err) {
      console.error('[SERVER] Failed to save the known incidents', err);
      res.send({
        error: 'Failed to save the known incident'
      });
    } else {
      res.send({
        result: ret
      });
    }
  });
});



module.exports = router;